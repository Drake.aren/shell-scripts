#!/bin/sh

source /etc/os-release

while read line; do
  case "$line" in
    \#*) continue ;;
    *) list="$list $line"
  esac
done < ./requirement.list

case $ID_LIKE in
  archlinux)
    pacman -S $list
  ;;
esac

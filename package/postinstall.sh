#!/bin/sh

# Export path's and define os typ
source $HOME/.config/envrc

function update_package_database {
    # debian
    apt update
}

function install {
    # debian
    apt install $1
}

# Git
install git

# Linux brew
git clone https://github.com/Linuxbrew/brew.git /home/linuxbrew/.linuxbrew

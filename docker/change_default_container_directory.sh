#!/bin/sh

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
  echo "[!]: This script must be run as root"
  exit 1
fi

if [ -n $1 ]; then
    echo "distenation dir dosen't specified"
    exit 1
fi

NEW_DOCKER_ROOT_DIR=$1

# if default path to docker is diffrent
# change this
OLD_DOCKER_ROOT_DIR=/var/lib/docker

mv $OLD_DOCKER_ROOT_DIR $NEW_DOCKER_ROOT_DIR
ln -s $NEW_DOCKER_ROOT_DIR $OLD_DOCKER_ROOT_DIR


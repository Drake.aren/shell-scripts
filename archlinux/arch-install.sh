#!/bin/sh 

# setup mirrors

SDA_KEYFILE=sda-keyfile
SDB_KEYFILE=sdb-keyfile

SDB_MAPPED_DEVICE=0
SDA_MAPPED_DEVICE=0

echo '[!]: Only for UEFI'
echo '[?]: Check internet connection'

if ! ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
	echo 'Network is down'
	exit 1
fi

echo 'Server = http://mirror.ps.kz/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist

echo '[+]: Set time zone (default=Asia/ALmaty)'

timedatectl set-ntp true
timedatectl set-timezone Asia/Almaty

echo '[+]: Parted'

echo '[+]: /dev/sdb'
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | gdisk /dev/sdb
  o 	# clear the in memory partition table
  y # confirm parted
  n 	# new partition
    	# partition number 'default'
    	# don't set offset
  +512M # 512 MB boot parttion
  ef00	# type of partition to 'EFI System'
  n 	# new partition
   	# partion number 'default'
	# don't set offset
	# take full disk size

  w 	# write the partition table
  y	# confirm parted
EOF

echo '[+]: /dev/sda'
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | gdisk /dev/sda
  o # clear the in memory partition table
  y # confirm parted
  n # new partition
    # partition number 'default'
    # default - start at beginning of disk 
    # take full partition size

  w # write the partition table
  y # confirm parted
EOF

echo '[+]: Success'
echo '[+]: Crypt'
echo "[+]: Creating keyfile's"

dd if=/dev/urandom of=./sda-keyfile bs=1024 count=4
chmod 0400 ./sda-keyfile 
cp ./sda-keyfile ./usb/

dd if=/dev/urandom of=./sdb-keyfile bs=1024 count=4 
chmod 0400 ./sdb-keyfile 
cp ./sdb-keyfile ./usb/

echo '[+]: Prepare LUKS'

cryptsetup luksFormat /dev/sda1 ./sda-keyfile

cryptsetup luksFormat /dev/sdb2 ./sdb-keyfile
 
echo '[+]: Maping LUKS'

cryptsetup luksOpen --key-file ./sda-keyfile /dev/sda1 sda-container

cryptsetup luksOpen --key-file ./sdb-keyfile /dev/sdb2 sdb-container

echo '[+]: Success'

ls -l /dev/mapper | grep container

echo '[+]: Setup LVM' 

pvcreate /dev/mapper/sdb-container
vgcreate ssdvg /dev/mapper/sdb-container
lvcreate -L4G -n swap ssdvg
lvcreate -l100%FREE -n root ssdvg

pvcreate /dev/mapper/sda-container
vgcreate hddvg /dev/mapper/sda-container
lvcreate -L15G -n var hddvg
lvcreate -L10G -n tmp hddvg
lvcreate -l100%FREE -n home hddvg

echo '[+]: Success'
echo '[+]: MakeFS'

# UEFI boot partition
mkfs.vfat /dev/sdb1

mkfs.ext4 -L root /dev/mapper/ssdvg-root
mkswap -L swap /dev/mapper/ssdvg-swap

mkfs.ext4 -L home /dev/mapper/hddvg-home
mkfs.ext4 -L var /dev/mapper/hddvg-var
mkfs.ext4 -L tmp /dev/mapper/hddvg-tmp

echo '[+]: Success'
echo '[+]: Mounting'

mount /dev/mapper/ssdvg-root /mnt/
mkdir -p /mnt/{home,var,tmp,boot/efi}

mount /dev/sdb1 /mnt/boot/efi/
mount /dev/mapper/hddvg-home /mnt/home/
mount /dev/mapper/hddvg-var /mnt/var/
mount /dev/mapper/hddvg-tmp /mnt/tmp/
swapon /dev/mapper/rootvg-swap

echo '[+]: Success'
lsblk

echo '[+]: Setup core system'

pacstrap /mnt base base-devel
genfstab -pU /mnt >> /mnt/etc/fstab

echo '[+]: Enter chroot'
 
cat << EOF | arch-chroot /mnt

ln -sf /usr/share/zoneinfo/Asia/Almaty /etc/localtime
hwclock --systohc
pacman -S wifi-menu wpa_supplicant dialog

echo '[+]: Configure host'

HOSTNAME=sw1542a4
LOCALDOMAIN=com
echo '$HOSTNAME' > /etc/hostname
echo '127.0.1.1 $HOSTNAME.LOCALDOMAIN $HOSTNAME' >> /etc/hosts

echo '[+]: Enter root passwdord'

passwd root

echo '
en_US ISO-8859-1
en_US.UTF-8 UTF-8
ru_RU.UTF-8 UTF-8
ru_RU ISO-8859-5' >> /etc/locale.gen

locale-gen

echo LANG=en_US.UTF-8 > /etc/locale.conf
echo KEYMAP=ru > /etc/vconsole.conf
echo FOND=cyr-sun16 >> /etc/vconsole.conf

sed -i '/HOOKS/c\HOOKS=(base udev autodetect modconf block keymap encrypt lvm2 resume filesystems keyboard fsck)' /etc/mkinitcpio.conf

mkinitcpio -p linux

echo '[+]: Setup GRUB'

pacman -S grub dosfstools efibootmgr mtools intel-ucode  

sed -i '/GRUB_ENABLE_CRYPTODISK/c\GRUB_ENABLE_CRYPTODISK=y' /etc/default/grub

SDA1_UUID=$(blkid -o value -s UUID /dev/sda1)
SDB2_UUID=$(blkid -o value -s UUID /dev/sdb2)

sed -i '/GRUB_CMDLINE_LINUX/c\GRUB_CMDLINE_LINUX="cryptdevice=UUID=${SDA1_UUID}:sda-container cryptdevice=UUID=${SDB2_UUID}:sdb-container"' /etc/default/grub

grub-mkconfig -o /boot/grub/grub.cfg

grub-install /dev/sdb

echo '[+]: Setup auto decrypt volume'

USBKEY_UUID=F08CDC1F8CDBDE62

echo 'UUID=$USBKEY_UUID /mnt/usbkey ext4 defaults,nofail,x-systemd.device-timeout=1' >> /etc/fstab

echo 'sda-container /dev/sda1 /mnt/usbkey/sda-keyfile luks,nofail' >> /etc/crypttab
echo 'sdb-container /dev/sdb2 /mnt/usbkey/sdb-keyfile luks,nofail' >> /etc/crypttab

sed -i '/CRYPTDISKS_MOUNT/c\CRYPTDISKS_MOUNT="/mnt/usbkey"' /etc/default/cryptdisks

echo '[+]: Exit chroot'
exit

EOF

echo '[+]: Done!'

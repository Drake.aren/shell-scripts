#! /bin/sh

TIME=$1
COMMAND=$2

while [ 1 ]
do
  DATE=`/bin/date +%H:%M`
  if [ $DATE. = $TIME. ]
  then
    $COMMAND
    exit 0
  fi
  sleep 60
done

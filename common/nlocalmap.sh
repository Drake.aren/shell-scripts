#!/bin/sh

$NETWORKMASK=$1

nmap -sn $NETWORKMASK | \
  egrep "scan report" | \
  awk '{print $5}

#!/bin/bash
set -e

function access_check {
  if [[ $EUID -ne 0 ]]; then
    ansi --red "[!]: Permision denied"
    ansi "[ ]: Please run as root"
    ansi "[ ]: exit with code 1"
    exit 1
  fi
}

access_check

FILE=/sys/class/backlight/intel_backlight/brightness

if [[ ! -f $FILE ]]; then
    ansi --red "[!]: file $FILE dons't exist"
    ansi "[ ]: exit with code 1"
    exit 1
fi

function help {
  ansi "USAGE: $0 [-dec|-inc] VALUE"
  ansi "  -dec : to decise brightness"
  ansi "  -inc : to increase brightness"
}

function change_brightness {
  sudo tee $FILE <<< $1
}

CURRENT=$(cat $FILE)
NEW=$2

case $1 in
  "-inc")
    change_brightness $(( CURRENT + NEW ))
  ;;
  "-dec")
    change_brightness $(( CURRENT - NEW ))
  ;;
  *)
    help
esac

exit 0
